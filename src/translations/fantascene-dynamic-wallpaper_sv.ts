<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="sv_SE">
<context>
    <name>Application</name>
    <message>
        <location filename="../application.cpp" line="146"/>
        <location filename="../application.cpp" line="147"/>
        <source>fantascene-dynamic-wallpaper</source>
        <translation>fantascen-dynamic-wallpaper</translation>
    </message>
</context>
<context>
    <name>Desktop</name>
    <message>
        <location filename="../desktop.ui" line="14"/>
        <source>Form</source>
        <translation>Formulär</translation>
    </message>
</context>
<context>
    <name>DownloadWidget</name>
    <message>
        <location filename="../download/downloadwidget.ui" line="14"/>
        <source>Form</source>
        <translation type="unfinished">Formulär</translation>
    </message>
    <message>
        <location filename="../download/downloadwidget.ui" line="25"/>
        <source>Load</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../download/downloadwidget.cpp" line="11"/>
        <source>The download address comes from the network and may not be usable</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>HelpDialog</name>
    <message>
        <location filename="../help/helpdialog.ui" line="14"/>
        <source>Dialog</source>
        <translation type="unfinished">Dialogruta</translation>
    </message>
    <message>
        <location filename="../help/helpdialog.ui" line="53"/>
        <source>Close</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../help/helpdialog.cpp" line="9"/>
        <source>Help</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>IconView</name>
    <message>
        <location filename="../desktop/iconview.cpp" line="183"/>
        <source>Open</source>
        <translation>Öppna</translation>
    </message>
    <message>
        <location filename="../desktop/iconview.cpp" line="188"/>
        <source>Open With</source>
        <translation>Öppna med</translation>
    </message>
    <message>
        <location filename="../desktop/iconview.cpp" line="193"/>
        <source>New Built</source>
        <translation>Nybyggd</translation>
    </message>
    <message>
        <location filename="../desktop/iconview.cpp" line="198"/>
        <source>Select all</source>
        <translation>Välj alla</translation>
    </message>
    <message>
        <location filename="../desktop/iconview.cpp" line="203"/>
        <source>Open Terminal</source>
        <translation>Öppna terminal</translation>
    </message>
    <message>
        <location filename="../desktop/iconview.cpp" line="209"/>
        <location filename="../desktop/iconview.cpp" line="933"/>
        <source>New Folder</source>
        <translation>Ny katalog</translation>
    </message>
    <message>
        <location filename="../desktop/iconview.cpp" line="215"/>
        <source>Sort Order</source>
        <translation>Sorteringsordning</translation>
    </message>
    <message>
        <location filename="../desktop/iconview.cpp" line="219"/>
        <source>Refresh Sort</source>
        <translation>Uppdatera sortering</translation>
    </message>
    <message>
        <location filename="../desktop/iconview.cpp" line="223"/>
        <source>Name</source>
        <translation>Namn</translation>
    </message>
    <message>
        <location filename="../desktop/iconview.cpp" line="227"/>
        <source>Change Date</source>
        <translation>Ändra datum</translation>
    </message>
    <message>
        <location filename="../desktop/iconview.cpp" line="231"/>
        <source>File Size</source>
        <translation>Filstorlek</translation>
    </message>
    <message>
        <location filename="../desktop/iconview.cpp" line="235"/>
        <source>File Type</source>
        <translation>Filtyp</translation>
    </message>
    <message>
        <location filename="../desktop/iconview.cpp" line="240"/>
        <source>Icon Size</source>
        <translation>Ikonstorlek</translation>
    </message>
    <message>
        <location filename="../desktop/iconview.cpp" line="244"/>
        <source>Small</source>
        <translation>Liten</translation>
    </message>
    <message>
        <location filename="../desktop/iconview.cpp" line="248"/>
        <source>Medium</source>
        <translation>Medel</translation>
    </message>
    <message>
        <location filename="../desktop/iconview.cpp" line="252"/>
        <source>Big</source>
        <translation>Stor</translation>
    </message>
    <message>
        <location filename="../desktop/iconview.cpp" line="258"/>
        <source>Copy</source>
        <translation>Kopiera</translation>
    </message>
    <message>
        <location filename="../desktop/iconview.cpp" line="263"/>
        <source>Cut</source>
        <translation>Klipp</translation>
    </message>
    <message>
        <location filename="../desktop/iconview.cpp" line="268"/>
        <source>Paste</source>
        <translation>Klistra in</translation>
    </message>
    <message>
        <location filename="../desktop/iconview.cpp" line="273"/>
        <source>Rename</source>
        <translation>Byt namn på</translation>
    </message>
    <message>
        <location filename="../desktop/iconview.cpp" line="278"/>
        <source>Trash</source>
        <translation>Papperskorgen</translation>
    </message>
    <message>
        <location filename="../desktop/iconview.cpp" line="283"/>
        <source>Set Wallpaper</source>
        <translation>Ange bakgrundsbild</translation>
    </message>
    <message>
        <location filename="../desktop/iconview.cpp" line="296"/>
        <source>New TXT</source>
        <translation>Ny TXT</translation>
    </message>
    <message>
        <location filename="../desktop/iconview.cpp" line="365"/>
        <source>Select Other Application</source>
        <translation>Välj annat program</translation>
    </message>
    <message>
        <location filename="../desktop/iconview.cpp" line="489"/>
        <source>Please Select App</source>
        <translation>Välj app</translation>
    </message>
    <message>
        <location filename="../desktop/iconview.cpp" line="947"/>
        <source>New Txt Files</source>
        <translation>Nya Txt- filer</translation>
    </message>
</context>
<context>
    <name>LocalWidget</name>
    <message>
        <location filename="../listview/localwidget.ui" line="14"/>
        <source>Form</source>
        <translation>Formulär</translation>
    </message>
    <message>
        <location filename="../listview/localwidget.ui" line="20"/>
        <source>Local</source>
        <translation>Lokal</translation>
    </message>
    <message>
        <location filename="../listview/localwidget.ui" line="45"/>
        <source>Play</source>
        <translation>Spela</translation>
    </message>
    <message>
        <location filename="../listview/localwidget.cpp" line="31"/>
        <source>Wallpaper Local</source>
        <translation>Bakgrundsbild lokalt</translation>
    </message>
    <message>
        <location filename="../listview/localwidget.cpp" line="40"/>
        <source>Please place the local video on:</source>
        <translation>Placera den lokala videon på:</translation>
    </message>
</context>
<context>
    <name>MoreSetting</name>
    <message>
        <location filename="../moresetting.ui" line="14"/>
        <source>Form</source>
        <translation>Formulär</translation>
    </message>
    <message>
        <location filename="../moresetting.ui" line="22"/>
        <source>When desktop is hidden</source>
        <translation>När skrivbordet är dolt</translation>
    </message>
    <message>
        <location filename="../moresetting.ui" line="30"/>
        <location filename="../moresetting.cpp" line="46"/>
        <location filename="../moresetting.cpp" line="112"/>
        <source>continue playing</source>
        <translation>fortsätt spela</translation>
    </message>
    <message>
        <location filename="../moresetting.ui" line="35"/>
        <location filename="../moresetting.cpp" line="48"/>
        <source>pause</source>
        <translation>paus</translation>
    </message>
    <message>
        <location filename="../moresetting.ui" line="47"/>
        <source>Video FPS</source>
        <translation>Video FPS</translation>
    </message>
    <message>
        <location filename="../moresetting.ui" line="55"/>
        <location filename="../moresetting.ui" line="357"/>
        <location filename="../moresetting.cpp" line="53"/>
        <location filename="../moresetting.cpp" line="120"/>
        <source>default</source>
        <translation>standard</translation>
    </message>
    <message>
        <location filename="../moresetting.ui" line="60"/>
        <source>10</source>
        <translation>tio</translation>
    </message>
    <message>
        <location filename="../moresetting.ui" line="65"/>
        <source>15</source>
        <translation>femton</translation>
    </message>
    <message>
        <location filename="../moresetting.ui" line="70"/>
        <source>20</source>
        <translation>tjugo</translation>
    </message>
    <message>
        <location filename="../moresetting.ui" line="75"/>
        <source>24</source>
        <translation>tjugofyra</translation>
    </message>
    <message>
        <location filename="../moresetting.ui" line="80"/>
        <source>25</source>
        <translation>tjugofem</translation>
    </message>
    <message>
        <location filename="../moresetting.ui" line="85"/>
        <source>30</source>
        <translation>trettio</translation>
    </message>
    <message>
        <location filename="../moresetting.ui" line="90"/>
        <source>60</source>
        <translation>sextio</translation>
    </message>
    <message>
        <location filename="../moresetting.ui" line="102"/>
        <source>Decoder</source>
        <translation>Dekodare</translation>
    </message>
    <message>
        <location filename="../moresetting.ui" line="110"/>
        <source>auto</source>
        <translation>auto</translation>
    </message>
    <message>
        <location filename="../moresetting.ui" line="115"/>
        <source>gpu</source>
        <translation>gpu</translation>
    </message>
    <message>
        <location filename="../moresetting.ui" line="120"/>
        <source>vaapi</source>
        <translation>vaapi</translation>
    </message>
    <message>
        <location filename="../moresetting.ui" line="125"/>
        <source>vdpau</source>
        <translation>vdpau</translation>
    </message>
    <message>
        <location filename="../moresetting.ui" line="130"/>
        <source>none</source>
        <translation>ingen</translation>
    </message>
    <message>
        <location filename="../moresetting.ui" line="135"/>
        <location filename="../moresetting.ui" line="185"/>
        <location filename="../moresetting.cpp" line="64"/>
        <location filename="../moresetting.cpp" line="77"/>
        <location filename="../moresetting.cpp" line="127"/>
        <location filename="../moresetting.cpp" line="134"/>
        <location filename="../moresetting.cpp" line="191"/>
        <location filename="../moresetting.cpp" line="218"/>
        <source>other</source>
        <translation>Annat</translation>
    </message>
    <message>
        <location filename="../moresetting.ui" line="157"/>
        <source>VO</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../moresetting.ui" line="165"/>
        <source>libmpv</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../moresetting.ui" line="175"/>
        <source>opengl</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../moresetting.ui" line="180"/>
        <source>opengl-cb</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../moresetting.ui" line="207"/>
        <source>Show desktop icon</source>
        <translation>Visa skrivbordsikon</translation>
    </message>
    <message>
        <location filename="../moresetting.ui" line="215"/>
        <location filename="../moresetting.ui" line="240"/>
        <location filename="../moresetting.ui" line="337"/>
        <location filename="../moresetting.cpp" line="83"/>
        <location filename="../moresetting.cpp" line="89"/>
        <location filename="../moresetting.cpp" line="95"/>
        <location filename="../moresetting.cpp" line="141"/>
        <location filename="../moresetting.cpp" line="148"/>
        <location filename="../moresetting.cpp" line="162"/>
        <source>true</source>
        <translation>sant</translation>
    </message>
    <message>
        <location filename="../moresetting.ui" line="220"/>
        <location filename="../moresetting.ui" line="245"/>
        <location filename="../moresetting.ui" line="332"/>
        <location filename="../moresetting.cpp" line="85"/>
        <location filename="../moresetting.cpp" line="91"/>
        <location filename="../moresetting.cpp" line="99"/>
        <source>false</source>
        <translation>falsk</translation>
    </message>
    <message>
        <location filename="../moresetting.ui" line="232"/>
        <source>Place on top of the original desktop</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../moresetting.ui" line="257"/>
        <source>Desktop transparency </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../moresetting.ui" line="264"/>
        <source>xx</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../moresetting.ui" line="294"/>
        <source>Wallpaper transparency </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../moresetting.ui" line="324"/>
        <source>Event penetration</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../moresetting.ui" line="349"/>
        <source>Spark Desktop font color</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../moresetting.ui" line="362"/>
        <source>white</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../moresetting.ui" line="367"/>
        <source>black</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../moresetting.ui" line="372"/>
        <source>blue</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../moresetting.ui" line="377"/>
        <source>yellow</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../moresetting.ui" line="382"/>
        <source>darkGray</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../moresetting.ui" line="387"/>
        <source>lightGray</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../moresetting.ui" line="392"/>
        <source>gray</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../moresetting.ui" line="397"/>
        <source>green</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../moresetting.ui" line="402"/>
        <source>cyan</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../moresetting.ui" line="407"/>
        <source>magenta</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../moresetting.ui" line="412"/>
        <source>transparent</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../moresetting.ui" line="417"/>
        <source>darkRed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../moresetting.ui" line="422"/>
        <source>darkGreen</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../moresetting.ui" line="427"/>
        <source>darkBlue</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../moresetting.ui" line="432"/>
        <source>darkCyan</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../moresetting.ui" line="437"/>
        <source>darkMagenta</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../moresetting.ui" line="442"/>
        <source>darkYellow</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../moresetting.ui" line="454"/>
        <source>Confirm</source>
        <translation>Bekräfta</translation>
    </message>
    <message>
        <location filename="../moresetting.ui" line="461"/>
        <source>Cancel</source>
        <translation>Avbryt</translation>
    </message>
    <message>
        <location filename="../moresetting.cpp" line="33"/>
        <source>Advanced Settings</source>
        <translation>Avancerade inställningar</translation>
    </message>
</context>
<context>
    <name>OnlineClient</name>
    <message>
        <location filename="../listview/onlineclient.ui" line="14"/>
        <source>Form</source>
        <translation type="unfinished">Formulär</translation>
    </message>
    <message>
        <location filename="../listview/onlineclient.ui" line="27"/>
        <source>Recommend</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../listview/onlineclient.ui" line="34"/>
        <source>Status:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../listview/onlineclient.ui" line="48"/>
        <source>Try Connected</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../listview/onlineclient.ui" line="70"/>
        <source>If unable to download, please check if &apos;wget&apos; is installed on the command line.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../listview/onlineclient.ui" line="82"/>
        <source>Search</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../listview/onlineclient.ui" line="100"/>
        <source>Page</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../listview/onlineclient.ui" line="119"/>
        <location filename="../listview/onlineclient.ui" line="157"/>
        <source>0</source>
        <translation type="unfinished">en punkt tre tre {0?}</translation>
    </message>
    <message>
        <location filename="../listview/onlineclient.ui" line="138"/>
        <source>/</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../listview/onlineclient.ui" line="170"/>
        <source>&lt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../listview/onlineclient.ui" line="183"/>
        <source>&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../listview/onlineclient.ui" line="200"/>
        <source>To</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../listview/onlineclient.ui" line="234"/>
        <source>For assistance with uploading wallpaper files or any other support, please send an email to either liuminghang0821@gmail.com or 523633637@qq.com.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../listview/onlineclient.ui" line="253"/>
        <source>The content above is sourced from the internet. If any content infringes upon your rights, please send an email to either liuminghang0821@gmail.com or 523633637@qq.com to request removal. We apologize for any inconvenience.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../listview/onlineclient.ui" line="286"/>
        <source>Name</source>
        <translation type="unfinished">Namn</translation>
    </message>
    <message>
        <location filename="../listview/onlineclient.ui" line="308"/>
        <location filename="../listview/onlineclient.ui" line="353"/>
        <location filename="../listview/onlineclient.ui" line="395"/>
        <location filename="../listview/onlineclient.ui" line="437"/>
        <location filename="../listview/onlineclient.ui" line="479"/>
        <location filename="../listview/onlineclient.ui" line="521"/>
        <location filename="../listview/onlineclient.ui" line="563"/>
        <location filename="../listview/onlineclient.ui" line="605"/>
        <location filename="../listview/onlineclient.ui" line="647"/>
        <source>Unknown</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../listview/onlineclient.ui" line="331"/>
        <source>Description</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../listview/onlineclient.ui" line="376"/>
        <source>Category</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../listview/onlineclient.ui" line="418"/>
        <source>Author</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../listview/onlineclient.ui" line="460"/>
        <source>Download Count</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../listview/onlineclient.ui" line="502"/>
        <source>File Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../listview/onlineclient.ui" line="544"/>
        <source>File Size</source>
        <translation type="unfinished">Filstorlek</translation>
    </message>
    <message>
        <location filename="../listview/onlineclient.ui" line="586"/>
        <source>Width</source>
        <translation type="unfinished">Bredd</translation>
    </message>
    <message>
        <location filename="../listview/onlineclient.ui" line="628"/>
        <source>Height</source>
        <translation type="unfinished">Höjd</translation>
    </message>
    <message>
        <location filename="../listview/onlineclient.ui" line="731"/>
        <source>Download</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../listview/onlineclient.cpp" line="23"/>
        <source>Fantascene Hub</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../listview/onlineclient.cpp" line="171"/>
        <location filename="../listview/onlineclient.cpp" line="237"/>
        <source>Dowload Ing.....</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../listview/onlineclient.cpp" line="192"/>
        <location filename="../listview/onlineclient.cpp" line="198"/>
        <location filename="../listview/onlineclient.cpp" line="294"/>
        <source>Error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../listview/onlineclient.cpp" line="192"/>
        <location filename="../listview/onlineclient.cpp" line="294"/>
        <source>Dowlaod Error!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../listview/onlineclient.cpp" line="198"/>
        <source>File Exists!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../listview/onlineclient.cpp" line="431"/>
        <source>Online</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../listview/onlineclient.ui" line="41"/>
        <location filename="../listview/onlineclient.cpp" line="437"/>
        <source>Not Online</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PlaylistSettingDialog</name>
    <message>
        <location filename="../listview/playlistsettingdialog.ui" line="14"/>
        <source>Dialog</source>
        <translation>Dialogruta</translation>
    </message>
    <message>
        <location filename="../listview/playlistsettingdialog.ui" line="22"/>
        <source>Timer(second)</source>
        <translation>Timer( sekund)</translation>
    </message>
    <message>
        <location filename="../listview/playlistsettingdialog.ui" line="31"/>
        <location filename="../listview/playlistsettingdialog.ui" line="45"/>
        <location filename="../listview/playlistsettingdialog.ui" line="71"/>
        <source>600</source>
        <translation>sexhundra</translation>
    </message>
    <message>
        <location filename="../listview/playlistsettingdialog.ui" line="35"/>
        <source>60</source>
        <translation>sextio</translation>
    </message>
    <message>
        <location filename="../listview/playlistsettingdialog.ui" line="40"/>
        <source>300</source>
        <translation>tre hundra</translation>
    </message>
    <message>
        <location filename="../listview/playlistsettingdialog.ui" line="50"/>
        <source>1800</source>
        <translation>ett tusen åtta hundra</translation>
    </message>
    <message>
        <location filename="../listview/playlistsettingdialog.ui" line="55"/>
        <source>3600</source>
        <translation>tre tusen sex hundra</translation>
    </message>
    <message>
        <location filename="../listview/playlistsettingdialog.ui" line="60"/>
        <location filename="../listview/playlistsettingdialog.cpp" line="11"/>
        <location filename="../listview/playlistsettingdialog.cpp" line="15"/>
        <location filename="../listview/playlistsettingdialog.cpp" line="27"/>
        <location filename="../listview/playlistsettingdialog.cpp" line="30"/>
        <source>other</source>
        <translation>Annat</translation>
    </message>
    <message>
        <location filename="../listview/playlistsettingdialog.ui" line="84"/>
        <source>Ok</source>
        <translation>Ok</translation>
    </message>
    <message>
        <location filename="../listview/playlistsettingdialog.ui" line="91"/>
        <source>Cancel</source>
        <translation>Avbryt</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../main.cpp" line="100"/>
        <source>fantacy</source>
        <translation>fantasier</translation>
    </message>
    <message>
        <location filename="../download/downloadwidget.cpp" line="33"/>
        <source>Download</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Wallpaper</name>
    <message>
        <location filename="../wallpaper.cpp" line="435"/>
        <source>Copy</source>
        <translation>Kopiera</translation>
    </message>
    <message>
        <location filename="../wallpaper.cpp" line="440"/>
        <source>Extend</source>
        <translation>Utvidga</translation>
    </message>
    <message>
        <location filename="../wallpaper.cpp" line="445"/>
        <source>Manual</source>
        <translation>Manuell</translation>
    </message>
</context>
<context>
    <name>historyWidget</name>
    <message>
        <location filename="../listview/historywidget.ui" line="14"/>
        <source>Form</source>
        <translation>Formulär</translation>
    </message>
    <message>
        <location filename="../listview/historywidget.ui" line="34"/>
        <source>History</source>
        <translation>Historik</translation>
    </message>
    <message>
        <location filename="../listview/historywidget.ui" line="46"/>
        <source>Set Wallpaper</source>
        <translation>Ange bakgrundsbild</translation>
    </message>
    <message>
        <location filename="../listview/historywidget.ui" line="53"/>
        <source>Set Wallpaper 2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../listview/historywidget.ui" line="60"/>
        <location filename="../listview/historywidget.ui" line="107"/>
        <source>Delete</source>
        <translation>Ta bort</translation>
    </message>
    <message>
        <location filename="../listview/historywidget.ui" line="67"/>
        <source>Import</source>
        <translation>Importera</translation>
    </message>
    <message>
        <location filename="../listview/historywidget.ui" line="88"/>
        <source>Playlist</source>
        <translation>Spellista</translation>
    </message>
    <message>
        <location filename="../listview/historywidget.ui" line="100"/>
        <source>Add Playlist</source>
        <translation>Lägg till spellista</translation>
    </message>
    <message>
        <location filename="../listview/historywidget.ui" line="114"/>
        <source>Loop play</source>
        <translation>Loop play</translation>
    </message>
    <message>
        <location filename="../listview/historywidget.ui" line="121"/>
        <source>Loop playback settings</source>
        <translation>Inställningar för loopuppspelning</translation>
    </message>
    <message>
        <location filename="../listview/historywidget.ui" line="212"/>
        <source>Name:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../listview/historywidget.ui" line="254"/>
        <source>Path:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../listview/historywidget.ui" line="290"/>
        <source>Thumbnail Path:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../listview/historywidget.ui" line="345"/>
        <source>Reload images</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../listview/historywidget.ui" line="352"/>
        <source>Select image as thumbnail</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../listview/historywidget.cpp" line="19"/>
        <source>Wallpaper History</source>
        <translation>Bakgrundshistorik</translation>
    </message>
    <message>
        <location filename="../listview/historywidget.cpp" line="78"/>
        <location filename="../listview/historywidget.cpp" line="140"/>
        <source>Delete!!</source>
        <translation>Ta bort!!</translation>
    </message>
    <message>
        <location filename="../listview/historywidget.cpp" line="78"/>
        <source>Delete all history imports ?</source>
        <translation>Ta bort all historikimport?</translation>
    </message>
    <message>
        <location filename="../listview/historywidget.cpp" line="140"/>
        <source>Delete all playback ?</source>
        <translation>Ta bort all uppspelning?</translation>
    </message>
</context>
<context>
    <name>settingWindow</name>
    <message>
        <location filename="../settingwindow.ui" line="14"/>
        <source>MainWindow</source>
        <translation>Huvudfönster</translation>
    </message>
    <message>
        <location filename="../settingwindow.ui" line="24"/>
        <location filename="../settingwindow.ui" line="42"/>
        <source>File</source>
        <translation>Arkiv</translation>
    </message>
    <message>
        <location filename="../settingwindow.ui" line="31"/>
        <location filename="../settingwindow.ui" line="49"/>
        <source>/usr/share/fantascene-dynamic-wallpaper/normal/normal.mp4</source>
        <translation>/usr/share/fantascene-dynamic-wallpaper/normal/normal.mp4</translation>
    </message>
    <message>
        <location filename="../settingwindow.ui" line="145"/>
        <source>Screen 2 independent playback</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settingwindow.ui" line="154"/>
        <source>Transparency</source>
        <translation>Öppenhet</translation>
    </message>
    <message>
        <location filename="../settingwindow.ui" line="181"/>
        <source>Volume</source>
        <translation>Volym</translation>
    </message>
    <message>
        <location filename="../settingwindow.ui" line="214"/>
        <location filename="../settingwindow.cpp" line="104"/>
        <source>Play</source>
        <translation>Spela</translation>
    </message>
    <message>
        <location filename="../settingwindow.ui" line="233"/>
        <location filename="../settingwindow.cpp" line="116"/>
        <source>Pause</source>
        <translation>Pausa</translation>
    </message>
    <message>
        <location filename="../settingwindow.ui" line="252"/>
        <source>Set New</source>
        <translation>Ange ny</translation>
    </message>
    <message>
        <location filename="../settingwindow.ui" line="265"/>
        <source>Set New 2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settingwindow.ui" line="284"/>
        <source>Hide</source>
        <translation>Dölj</translation>
    </message>
    <message>
        <location filename="../settingwindow.ui" line="299"/>
        <source>Model</source>
        <translation>Modell</translation>
    </message>
    <message>
        <location filename="../settingwindow.ui" line="307"/>
        <source>Copy</source>
        <translation>Kopiera</translation>
    </message>
    <message>
        <location filename="../settingwindow.ui" line="312"/>
        <source>Extend</source>
        <translation>Utvidga</translation>
    </message>
    <message>
        <location filename="../settingwindow.ui" line="317"/>
        <location filename="../settingwindow.cpp" line="686"/>
        <source>Manual</source>
        <translation>Manuell</translation>
    </message>
    <message>
        <location filename="../settingwindow.ui" line="325"/>
        <source>Add to startup</source>
        <translation>Lägg till vid start</translation>
    </message>
    <message>
        <location filename="../settingwindow.ui" line="336"/>
        <source>Video aspect ratio</source>
        <translation>Videobildförhållande</translation>
    </message>
    <message>
        <location filename="../settingwindow.ui" line="350"/>
        <location filename="../settingwindow.cpp" line="861"/>
        <source>default</source>
        <translation>standard</translation>
    </message>
    <message>
        <location filename="../settingwindow.ui" line="355"/>
        <source>4:3</source>
        <translation>4: 3</translation>
    </message>
    <message>
        <location filename="../settingwindow.ui" line="360"/>
        <source>16:9</source>
        <translation>16: 9</translation>
    </message>
    <message>
        <location filename="../settingwindow.ui" line="365"/>
        <source>16:10</source>
        <translation>16:10</translation>
    </message>
    <message>
        <location filename="../settingwindow.ui" line="370"/>
        <source>1.85:1</source>
        <translation>1.85:1</translation>
    </message>
    <message>
        <location filename="../settingwindow.ui" line="375"/>
        <source>2.35:1</source>
        <translation>2.35:1</translation>
    </message>
    <message>
        <location filename="../settingwindow.ui" line="380"/>
        <location filename="../settingwindow.cpp" line="873"/>
        <source>custom</source>
        <translation>anpassad</translation>
    </message>
    <message>
        <location filename="../settingwindow.ui" line="390"/>
        <source>1.33</source>
        <translation>en punkt tre tre</translation>
    </message>
    <message>
        <location filename="../settingwindow.ui" line="397"/>
        <source>Apply scale</source>
        <translation>Använd skala</translation>
    </message>
    <message>
        <location filename="../settingwindow.ui" line="416"/>
        <source>X</source>
        <translation>X</translation>
    </message>
    <message>
        <location filename="../settingwindow.ui" line="430"/>
        <source>Y</source>
        <translation>Y</translation>
    </message>
    <message>
        <location filename="../settingwindow.ui" line="444"/>
        <source>Width</source>
        <translation>Bredd</translation>
    </message>
    <message>
        <location filename="../settingwindow.ui" line="458"/>
        <source>Height</source>
        <translation>Höjd</translation>
    </message>
    <message>
        <location filename="../settingwindow.ui" line="472"/>
        <source>Apply Custom Dimensions</source>
        <translation>Använd anpassade mått</translation>
    </message>
    <message>
        <location filename="../settingwindow.ui" line="491"/>
        <source>githubWeb</source>
        <translation>githubWeb</translation>
    </message>
    <message>
        <location filename="../settingwindow.ui" line="498"/>
        <source>new(gitee)</source>
        <translation>new(gitee)</translation>
    </message>
    <message>
        <location filename="../settingwindow.ui" line="505"/>
        <source>new(github)</source>
        <translation>new( github)</translation>
    </message>
    <message>
        <location filename="../settingwindow.ui" line="512"/>
        <source>reprotBug</source>
        <translation>reprotBug</translation>
    </message>
    <message>
        <location filename="../settingwindow.ui" line="535"/>
        <source>Spark-Hub(Test)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settingwindow.ui" line="542"/>
        <source>Dowload Wallpaper</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settingwindow.ui" line="553"/>
        <source>Advanced Settings</source>
        <translation>Avancerade inställningar</translation>
    </message>
    <message>
        <location filename="../settingwindow.ui" line="560"/>
        <source>Wallpaper Local</source>
        <translation>Bakgrundsbild lokalt</translation>
    </message>
    <message>
        <location filename="../settingwindow.ui" line="567"/>
        <location filename="../settingwindow.cpp" line="123"/>
        <source>Wallpaper History</source>
        <translation>Bakgrundshistorik</translation>
    </message>
    <message>
        <location filename="../settingwindow.ui" line="574"/>
        <source>Wallpaper Engine-plugin</source>
        <translation>Insticksprogram för bakgrundsmotor</translation>
    </message>
    <message>
        <location filename="../settingwindow.cpp" line="90"/>
        <source>Exit</source>
        <translation>Avsluta</translation>
    </message>
    <message>
        <location filename="../settingwindow.cpp" line="111"/>
        <source>Screenshot</source>
        <translation>Skärmbild</translation>
    </message>
    <message>
        <location filename="../settingwindow.cpp" line="127"/>
        <source>Main View</source>
        <translation>Huvudvy</translation>
    </message>
    <message>
        <location filename="../settingwindow.cpp" line="144"/>
        <source>Help</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settingwindow.cpp" line="152"/>
        <source>github</source>
        <translation>github</translation>
    </message>
    <message>
        <location filename="../settingwindow.cpp" line="169"/>
        <source>Double click to open the configuration interface</source>
        <translation>Dubbelklicka för att öppna inställningsgränssnittet</translation>
    </message>
    <message>
        <location filename="../settingwindow.cpp" line="222"/>
        <source>Latest version</source>
        <translation>Senaste versionen</translation>
    </message>
</context>
<context>
    <name>view</name>
    <message>
        <location filename="../listview/view.cpp" line="244"/>
        <location filename="../listview/view.cpp" line="251"/>
        <source>Delete!!</source>
        <translation>Ta bort!!</translation>
    </message>
    <message>
        <location filename="../listview/view.cpp" line="244"/>
        <source>The file does not exist. Do you want to delete it</source>
        <translation>Filen finns inte. Vill du ta bort den</translation>
    </message>
    <message>
        <location filename="../listview/view.cpp" line="251"/>
        <source>Delete all history imports ?</source>
        <translation>Ta bort all historikimport?</translation>
    </message>
</context>
<context>
    <name>wallpaperEnginePlugin</name>
    <message>
        <location filename="../listview/wallpaperengineplugin.ui" line="14"/>
        <source>Form</source>
        <translation>Formulär</translation>
    </message>
    <message>
        <location filename="../listview/wallpaperengineplugin.ui" line="25"/>
        <source>Select Path</source>
        <translation>Välj sökväg</translation>
    </message>
    <message>
        <location filename="../listview/wallpaperengineplugin.ui" line="35"/>
        <source>Apply</source>
        <translation>Använd</translation>
    </message>
    <message>
        <location filename="../listview/wallpaperengineplugin.ui" line="56"/>
        <source>You have to provide the path of Wallpaper Engine from Steam. It doesn&apos;t work on Linux, but required functionality is present</source>
        <translation>Du måste tillhandahålla vägen till Wallpaper Engine från Steam. Det fungerar inte på Linux, men erforderlig funktionalitet är närvarande</translation>
    </message>
    <message>
        <location filename="../listview/wallpaperengineplugin.cpp" line="157"/>
        <location filename="../listview/wallpaperengineplugin.cpp" line="171"/>
        <source>Error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../listview/wallpaperengineplugin.cpp" line="157"/>
        <location filename="../listview/wallpaperengineplugin.cpp" line="171"/>
        <source>Not containing the word steam!</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>

<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="it_IT">
<context>
    <name>Application</name>
    <message>
        <location filename="../application.cpp" line="146"/>
        <location filename="../application.cpp" line="147"/>
        <source>fantascene-dynamic-wallpaper</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Desktop</name>
    <message>
        <location filename="../desktop.ui" line="14"/>
        <source>Form</source>
        <translation>finestra</translation>
    </message>
</context>
<context>
    <name>DownloadWidget</name>
    <message>
        <location filename="../download/downloadwidget.ui" line="14"/>
        <source>Form</source>
        <translation type="unfinished">finestra</translation>
    </message>
    <message>
        <location filename="../download/downloadwidget.ui" line="25"/>
        <source>Load</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../download/downloadwidget.cpp" line="11"/>
        <source>The download address comes from the network and may not be usable</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>HelpDialog</name>
    <message>
        <location filename="../help/helpdialog.ui" line="14"/>
        <source>Dialog</source>
        <translation type="unfinished">finestra</translation>
    </message>
    <message>
        <location filename="../help/helpdialog.ui" line="53"/>
        <source>Close</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../help/helpdialog.cpp" line="9"/>
        <source>Help</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>IconView</name>
    <message>
        <location filename="../desktop/iconview.cpp" line="183"/>
        <source>Open</source>
        <translation>aperto</translation>
    </message>
    <message>
        <location filename="../desktop/iconview.cpp" line="188"/>
        <source>Open With</source>
        <translation>Apri con</translation>
    </message>
    <message>
        <location filename="../desktop/iconview.cpp" line="193"/>
        <source>New Built</source>
        <translation>Nuovo</translation>
    </message>
    <message>
        <location filename="../desktop/iconview.cpp" line="198"/>
        <source>Select all</source>
        <translation>Seleziona tutto</translation>
    </message>
    <message>
        <location filename="../desktop/iconview.cpp" line="203"/>
        <source>Open Terminal</source>
        <translation>Apri terminale</translation>
    </message>
    <message>
        <location filename="../desktop/iconview.cpp" line="209"/>
        <location filename="../desktop/iconview.cpp" line="933"/>
        <source>New Folder</source>
        <translation>Nuova cartella</translation>
    </message>
    <message>
        <location filename="../desktop/iconview.cpp" line="215"/>
        <source>Sort Order</source>
        <translation>Ordinamento file</translation>
    </message>
    <message>
        <location filename="../desktop/iconview.cpp" line="219"/>
        <source>Refresh Sort</source>
        <translation>Aggiorna ordinamento</translation>
    </message>
    <message>
        <location filename="../desktop/iconview.cpp" line="223"/>
        <source>Name</source>
        <translation>nome</translation>
    </message>
    <message>
        <location filename="../desktop/iconview.cpp" line="227"/>
        <source>Change Date</source>
        <translation>Tempo di modifica</translation>
    </message>
    <message>
        <location filename="../desktop/iconview.cpp" line="231"/>
        <source>File Size</source>
        <translation>dimensione file</translation>
    </message>
    <message>
        <location filename="../desktop/iconview.cpp" line="235"/>
        <source>File Type</source>
        <translation>tipo di file</translation>
    </message>
    <message>
        <location filename="../desktop/iconview.cpp" line="240"/>
        <source>Icon Size</source>
        <translation>Dimensione icona</translation>
    </message>
    <message>
        <location filename="../desktop/iconview.cpp" line="244"/>
        <source>Small</source>
        <translation>piccolo</translation>
    </message>
    <message>
        <location filename="../desktop/iconview.cpp" line="248"/>
        <source>Medium</source>
        <translation>in</translation>
    </message>
    <message>
        <location filename="../desktop/iconview.cpp" line="252"/>
        <source>Big</source>
        <translation>grande</translation>
    </message>
    <message>
        <location filename="../desktop/iconview.cpp" line="258"/>
        <source>Copy</source>
        <translation>copia</translation>
    </message>
    <message>
        <location filename="../desktop/iconview.cpp" line="263"/>
        <source>Cut</source>
        <translation>taglio</translation>
    </message>
    <message>
        <location filename="../desktop/iconview.cpp" line="268"/>
        <source>Paste</source>
        <translation>pasta</translation>
    </message>
    <message>
        <location filename="../desktop/iconview.cpp" line="273"/>
        <source>Rename</source>
        <translation>rinomina</translation>
    </message>
    <message>
        <location filename="../desktop/iconview.cpp" line="278"/>
        <source>Trash</source>
        <translation>elimina</translation>
    </message>
    <message>
        <location filename="../desktop/iconview.cpp" line="283"/>
        <source>Set Wallpaper</source>
        <translation>imposta sfondo</translation>
    </message>
    <message>
        <location filename="../desktop/iconview.cpp" line="296"/>
        <source>New TXT</source>
        <translation>Nuovo Txt</translation>
    </message>
    <message>
        <location filename="../desktop/iconview.cpp" line="365"/>
        <source>Select Other Application</source>
        <translation>Scegli un&apos;altra applicazione</translation>
    </message>
    <message>
        <location filename="../desktop/iconview.cpp" line="489"/>
        <source>Please Select App</source>
        <translation>Seleziona un&apos;applicazione</translation>
    </message>
    <message>
        <location filename="../desktop/iconview.cpp" line="947"/>
        <source>New Txt Files</source>
        <translation>Nuovo TXT</translation>
    </message>
</context>
<context>
    <name>LocalWidget</name>
    <message>
        <location filename="../listview/localwidget.ui" line="14"/>
        <source>Form</source>
        <translation>finestra</translation>
    </message>
    <message>
        <location filename="../listview/localwidget.ui" line="20"/>
        <source>Local</source>
        <translation>locale</translation>
    </message>
    <message>
        <location filename="../listview/localwidget.ui" line="45"/>
        <source>Play</source>
        <translation>gioca</translation>
    </message>
    <message>
        <location filename="../listview/localwidget.cpp" line="31"/>
        <source>Wallpaper Local</source>
        <translation>Sfondo locale</translation>
    </message>
    <message>
        <location filename="../listview/localwidget.cpp" line="40"/>
        <source>Please place the local video on:</source>
        <translation>Si prega di inserire lo sfondo locale a questo indirizzo:</translation>
    </message>
</context>
<context>
    <name>MoreSetting</name>
    <message>
        <location filename="../moresetting.ui" line="14"/>
        <source>Form</source>
        <translation>finestra</translation>
    </message>
    <message>
        <location filename="../moresetting.ui" line="22"/>
        <source>When desktop is hidden</source>
        <translation>Quando il desktop è nascosto</translation>
    </message>
    <message>
        <location filename="../moresetting.ui" line="30"/>
        <location filename="../moresetting.cpp" line="46"/>
        <location filename="../moresetting.cpp" line="112"/>
        <source>continue playing</source>
        <translation>Continua a giocare</translation>
    </message>
    <message>
        <location filename="../moresetting.ui" line="35"/>
        <location filename="../moresetting.cpp" line="48"/>
        <source>pause</source>
        <translation>sospendere</translation>
    </message>
    <message>
        <location filename="../moresetting.ui" line="47"/>
        <source>Video FPS</source>
        <translation>Velocità fotogramma</translation>
    </message>
    <message>
        <location filename="../moresetting.ui" line="55"/>
        <location filename="../moresetting.ui" line="357"/>
        <location filename="../moresetting.cpp" line="53"/>
        <location filename="../moresetting.cpp" line="120"/>
        <source>default</source>
        <translation>default</translation>
    </message>
    <message>
        <location filename="../moresetting.ui" line="60"/>
        <source>10</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../moresetting.ui" line="65"/>
        <source>15</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../moresetting.ui" line="70"/>
        <source>20</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../moresetting.ui" line="75"/>
        <source>24</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../moresetting.ui" line="80"/>
        <source>25</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../moresetting.ui" line="85"/>
        <source>30</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../moresetting.ui" line="90"/>
        <source>60</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../moresetting.ui" line="102"/>
        <source>Decoder</source>
        <translation>decodifica</translation>
    </message>
    <message>
        <location filename="../moresetting.ui" line="110"/>
        <source>auto</source>
        <translation>auto</translation>
    </message>
    <message>
        <location filename="../moresetting.ui" line="115"/>
        <source>gpu</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../moresetting.ui" line="120"/>
        <source>vaapi</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../moresetting.ui" line="125"/>
        <source>vdpau</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../moresetting.ui" line="130"/>
        <source>none</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../moresetting.ui" line="135"/>
        <location filename="../moresetting.ui" line="185"/>
        <location filename="../moresetting.cpp" line="64"/>
        <location filename="../moresetting.cpp" line="77"/>
        <location filename="../moresetting.cpp" line="127"/>
        <location filename="../moresetting.cpp" line="134"/>
        <location filename="../moresetting.cpp" line="191"/>
        <location filename="../moresetting.cpp" line="218"/>
        <source>other</source>
        <translation>altro</translation>
    </message>
    <message>
        <location filename="../moresetting.ui" line="157"/>
        <source>VO</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../moresetting.ui" line="165"/>
        <source>libmpv</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../moresetting.ui" line="175"/>
        <source>opengl</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../moresetting.ui" line="180"/>
        <source>opengl-cb</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../moresetting.ui" line="207"/>
        <source>Show desktop icon</source>
        <translation>Visualizza icona del desktop</translation>
    </message>
    <message>
        <location filename="../moresetting.ui" line="215"/>
        <location filename="../moresetting.ui" line="240"/>
        <location filename="../moresetting.ui" line="337"/>
        <location filename="../moresetting.cpp" line="83"/>
        <location filename="../moresetting.cpp" line="89"/>
        <location filename="../moresetting.cpp" line="95"/>
        <location filename="../moresetting.cpp" line="141"/>
        <location filename="../moresetting.cpp" line="148"/>
        <location filename="../moresetting.cpp" line="162"/>
        <source>true</source>
        <translation>sì</translation>
    </message>
    <message>
        <location filename="../moresetting.ui" line="220"/>
        <location filename="../moresetting.ui" line="245"/>
        <location filename="../moresetting.ui" line="332"/>
        <location filename="../moresetting.cpp" line="85"/>
        <location filename="../moresetting.cpp" line="91"/>
        <location filename="../moresetting.cpp" line="99"/>
        <source>false</source>
        <translation>no</translation>
    </message>
    <message>
        <location filename="../moresetting.ui" line="232"/>
        <source>Place on top of the original desktop</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../moresetting.ui" line="257"/>
        <source>Desktop transparency </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../moresetting.ui" line="264"/>
        <source>xx</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../moresetting.ui" line="294"/>
        <source>Wallpaper transparency </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../moresetting.ui" line="324"/>
        <source>Event penetration</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../moresetting.ui" line="349"/>
        <source>Spark Desktop font color</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../moresetting.ui" line="362"/>
        <source>white</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../moresetting.ui" line="367"/>
        <source>black</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../moresetting.ui" line="372"/>
        <source>blue</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../moresetting.ui" line="377"/>
        <source>yellow</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../moresetting.ui" line="382"/>
        <source>darkGray</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../moresetting.ui" line="387"/>
        <source>lightGray</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../moresetting.ui" line="392"/>
        <source>gray</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../moresetting.ui" line="397"/>
        <source>green</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../moresetting.ui" line="402"/>
        <source>cyan</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../moresetting.ui" line="407"/>
        <source>magenta</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../moresetting.ui" line="412"/>
        <source>transparent</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../moresetting.ui" line="417"/>
        <source>darkRed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../moresetting.ui" line="422"/>
        <source>darkGreen</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../moresetting.ui" line="427"/>
        <source>darkBlue</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../moresetting.ui" line="432"/>
        <source>darkCyan</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../moresetting.ui" line="437"/>
        <source>darkMagenta</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../moresetting.ui" line="442"/>
        <source>darkYellow</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../moresetting.ui" line="454"/>
        <source>Confirm</source>
        <translation>conferma</translation>
    </message>
    <message>
        <location filename="../moresetting.ui" line="461"/>
        <source>Cancel</source>
        <translation>annulla</translation>
    </message>
    <message>
        <location filename="../moresetting.cpp" line="33"/>
        <source>Advanced Settings</source>
        <translation>Interfaccia più impostazioni</translation>
    </message>
</context>
<context>
    <name>OnlineClient</name>
    <message>
        <location filename="../listview/onlineclient.ui" line="14"/>
        <source>Form</source>
        <translation type="unfinished">finestra</translation>
    </message>
    <message>
        <location filename="../listview/onlineclient.ui" line="27"/>
        <source>Recommend</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../listview/onlineclient.ui" line="34"/>
        <source>Status:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../listview/onlineclient.ui" line="48"/>
        <source>Try Connected</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../listview/onlineclient.ui" line="70"/>
        <source>If unable to download, please check if &apos;wget&apos; is installed on the command line.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../listview/onlineclient.ui" line="82"/>
        <source>Search</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../listview/onlineclient.ui" line="100"/>
        <source>Page</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../listview/onlineclient.ui" line="119"/>
        <location filename="../listview/onlineclient.ui" line="157"/>
        <source>0</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../listview/onlineclient.ui" line="138"/>
        <source>/</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../listview/onlineclient.ui" line="170"/>
        <source>&lt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../listview/onlineclient.ui" line="183"/>
        <source>&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../listview/onlineclient.ui" line="200"/>
        <source>To</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../listview/onlineclient.ui" line="234"/>
        <source>For assistance with uploading wallpaper files or any other support, please send an email to either liuminghang0821@gmail.com or 523633637@qq.com.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../listview/onlineclient.ui" line="253"/>
        <source>The content above is sourced from the internet. If any content infringes upon your rights, please send an email to either liuminghang0821@gmail.com or 523633637@qq.com to request removal. We apologize for any inconvenience.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../listview/onlineclient.ui" line="286"/>
        <source>Name</source>
        <translation type="unfinished">nome</translation>
    </message>
    <message>
        <location filename="../listview/onlineclient.ui" line="308"/>
        <location filename="../listview/onlineclient.ui" line="353"/>
        <location filename="../listview/onlineclient.ui" line="395"/>
        <location filename="../listview/onlineclient.ui" line="437"/>
        <location filename="../listview/onlineclient.ui" line="479"/>
        <location filename="../listview/onlineclient.ui" line="521"/>
        <location filename="../listview/onlineclient.ui" line="563"/>
        <location filename="../listview/onlineclient.ui" line="605"/>
        <location filename="../listview/onlineclient.ui" line="647"/>
        <source>Unknown</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../listview/onlineclient.ui" line="331"/>
        <source>Description</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../listview/onlineclient.ui" line="376"/>
        <source>Category</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../listview/onlineclient.ui" line="418"/>
        <source>Author</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../listview/onlineclient.ui" line="460"/>
        <source>Download Count</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../listview/onlineclient.ui" line="502"/>
        <source>File Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../listview/onlineclient.ui" line="544"/>
        <source>File Size</source>
        <translation type="unfinished">dimensione file</translation>
    </message>
    <message>
        <location filename="../listview/onlineclient.ui" line="586"/>
        <source>Width</source>
        <translation type="unfinished">larghezza</translation>
    </message>
    <message>
        <location filename="../listview/onlineclient.ui" line="628"/>
        <source>Height</source>
        <translation type="unfinished">altezza</translation>
    </message>
    <message>
        <location filename="../listview/onlineclient.ui" line="731"/>
        <source>Download</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../listview/onlineclient.cpp" line="23"/>
        <source>Fantascene Hub</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../listview/onlineclient.cpp" line="171"/>
        <location filename="../listview/onlineclient.cpp" line="237"/>
        <source>Dowload Ing.....</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../listview/onlineclient.cpp" line="192"/>
        <location filename="../listview/onlineclient.cpp" line="198"/>
        <location filename="../listview/onlineclient.cpp" line="294"/>
        <source>Error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../listview/onlineclient.cpp" line="192"/>
        <location filename="../listview/onlineclient.cpp" line="294"/>
        <source>Dowlaod Error!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../listview/onlineclient.cpp" line="198"/>
        <source>File Exists!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../listview/onlineclient.cpp" line="431"/>
        <source>Online</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../listview/onlineclient.ui" line="41"/>
        <location filename="../listview/onlineclient.cpp" line="437"/>
        <source>Not Online</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PlaylistSettingDialog</name>
    <message>
        <location filename="../listview/playlistsettingdialog.ui" line="14"/>
        <source>Dialog</source>
        <translation>finestra</translation>
    </message>
    <message>
        <location filename="../listview/playlistsettingdialog.ui" line="22"/>
        <source>Timer(second)</source>
        <translation>Tempo (secondi)</translation>
    </message>
    <message>
        <location filename="../listview/playlistsettingdialog.ui" line="31"/>
        <location filename="../listview/playlistsettingdialog.ui" line="45"/>
        <location filename="../listview/playlistsettingdialog.ui" line="71"/>
        <source>600</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../listview/playlistsettingdialog.ui" line="35"/>
        <source>60</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../listview/playlistsettingdialog.ui" line="40"/>
        <source>300</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../listview/playlistsettingdialog.ui" line="50"/>
        <source>1800</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../listview/playlistsettingdialog.ui" line="55"/>
        <source>3600</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../listview/playlistsettingdialog.ui" line="60"/>
        <location filename="../listview/playlistsettingdialog.cpp" line="11"/>
        <location filename="../listview/playlistsettingdialog.cpp" line="15"/>
        <location filename="../listview/playlistsettingdialog.cpp" line="27"/>
        <location filename="../listview/playlistsettingdialog.cpp" line="30"/>
        <source>other</source>
        <translation>altro</translation>
    </message>
    <message>
        <location filename="../listview/playlistsettingdialog.ui" line="84"/>
        <source>Ok</source>
        <translation>conferma</translation>
    </message>
    <message>
        <location filename="../listview/playlistsettingdialog.ui" line="91"/>
        <source>Cancel</source>
        <translation>annulla</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../main.cpp" line="100"/>
        <source>fantacy</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../download/downloadwidget.cpp" line="33"/>
        <source>Download</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Wallpaper</name>
    <message>
        <location filename="../wallpaper.cpp" line="435"/>
        <source>Copy</source>
        <translation>copia</translation>
    </message>
    <message>
        <location filename="../wallpaper.cpp" line="440"/>
        <source>Extend</source>
        <translation>estendere</translation>
    </message>
    <message>
        <location filename="../wallpaper.cpp" line="445"/>
        <source>Manual</source>
        <translation>Manuale</translation>
    </message>
</context>
<context>
    <name>historyWidget</name>
    <message>
        <location filename="../listview/historywidget.ui" line="14"/>
        <source>Form</source>
        <translation>finestra</translation>
    </message>
    <message>
        <location filename="../listview/historywidget.ui" line="34"/>
        <source>History</source>
        <translation>storia</translation>
    </message>
    <message>
        <location filename="../listview/historywidget.ui" line="46"/>
        <source>Set Wallpaper</source>
        <translation>imposta sfondo</translation>
    </message>
    <message>
        <location filename="../listview/historywidget.ui" line="53"/>
        <source>Set Wallpaper 2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../listview/historywidget.ui" line="60"/>
        <location filename="../listview/historywidget.ui" line="107"/>
        <source>Delete</source>
        <translation>elimina</translation>
    </message>
    <message>
        <location filename="../listview/historywidget.ui" line="67"/>
        <source>Import</source>
        <translation>Importa</translation>
    </message>
    <message>
        <location filename="../listview/historywidget.ui" line="88"/>
        <source>Playlist</source>
        <translation>Playlist loop</translation>
    </message>
    <message>
        <location filename="../listview/historywidget.ui" line="100"/>
        <source>Add Playlist</source>
        <translation>Join looping playlist</translation>
    </message>
    <message>
        <location filename="../listview/historywidget.ui" line="114"/>
        <source>Loop play</source>
        <translation>Indica se giocare in loop</translation>
    </message>
    <message>
        <location filename="../listview/historywidget.ui" line="121"/>
        <source>Loop playback settings</source>
        <translation>Impostazioni di riproduzione loop</translation>
    </message>
    <message>
        <location filename="../listview/historywidget.ui" line="212"/>
        <source>Name:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../listview/historywidget.ui" line="254"/>
        <source>Path:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../listview/historywidget.ui" line="290"/>
        <source>Thumbnail Path:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../listview/historywidget.ui" line="345"/>
        <source>Reload images</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../listview/historywidget.ui" line="352"/>
        <source>Select image as thumbnail</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../listview/historywidget.cpp" line="19"/>
        <source>Wallpaper History</source>
        <translation>Sfondo storico</translation>
    </message>
    <message>
        <location filename="../listview/historywidget.cpp" line="78"/>
        <location filename="../listview/historywidget.cpp" line="140"/>
        <source>Delete!!</source>
        <translation>Elimina!!</translation>
    </message>
    <message>
        <location filename="../listview/historywidget.cpp" line="78"/>
        <source>Delete all history imports ?</source>
        <translation>Vuoi eliminare tutto?</translation>
    </message>
    <message>
        <location filename="../listview/historywidget.cpp" line="140"/>
        <source>Delete all playback ?</source>
        <translation>Eliminare tutte le playlist in loop?</translation>
    </message>
</context>
<context>
    <name>settingWindow</name>
    <message>
        <location filename="../settingwindow.ui" line="14"/>
        <source>MainWindow</source>
        <translation>Modulo principale</translation>
    </message>
    <message>
        <location filename="../settingwindow.ui" line="24"/>
        <location filename="../settingwindow.ui" line="42"/>
        <source>File</source>
        <translation>Seleziona file</translation>
    </message>
    <message>
        <location filename="../settingwindow.ui" line="31"/>
        <location filename="../settingwindow.ui" line="49"/>
        <source>/usr/share/fantascene-dynamic-wallpaper/normal/normal.mp4</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settingwindow.ui" line="145"/>
        <source>Screen 2 independent playback</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settingwindow.ui" line="154"/>
        <source>Transparency</source>
        <translation>trasparenza</translation>
    </message>
    <message>
        <location filename="../settingwindow.ui" line="181"/>
        <source>Volume</source>
        <translation>volume</translation>
    </message>
    <message>
        <location filename="../settingwindow.ui" line="214"/>
        <location filename="../settingwindow.cpp" line="104"/>
        <source>Play</source>
        <translation>gioca</translation>
    </message>
    <message>
        <location filename="../settingwindow.ui" line="233"/>
        <location filename="../settingwindow.cpp" line="116"/>
        <source>Pause</source>
        <translation>sospendere</translation>
    </message>
    <message>
        <location filename="../settingwindow.ui" line="252"/>
        <source>Set New</source>
        <translation>Imposta un nuovo sfondo</translation>
    </message>
    <message>
        <location filename="../settingwindow.ui" line="265"/>
        <source>Set New 2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settingwindow.ui" line="284"/>
        <source>Hide</source>
        <translation>Nascondi finestra</translation>
    </message>
    <message>
        <location filename="../settingwindow.ui" line="299"/>
        <source>Model</source>
        <translation>modalità</translation>
    </message>
    <message>
        <location filename="../settingwindow.ui" line="307"/>
        <source>Copy</source>
        <translation>copia</translation>
    </message>
    <message>
        <location filename="../settingwindow.ui" line="312"/>
        <source>Extend</source>
        <translation>estendere</translation>
    </message>
    <message>
        <location filename="../settingwindow.ui" line="317"/>
        <location filename="../settingwindow.cpp" line="686"/>
        <source>Manual</source>
        <translation>Manuale</translation>
    </message>
    <message>
        <location filename="../settingwindow.ui" line="325"/>
        <source>Add to startup</source>
        <translation>Avvio automatico all&apos;avvio</translation>
    </message>
    <message>
        <location filename="../settingwindow.ui" line="336"/>
        <source>Video aspect ratio</source>
        <translation>Scala video</translation>
    </message>
    <message>
        <location filename="../settingwindow.ui" line="350"/>
        <location filename="../settingwindow.cpp" line="861"/>
        <source>default</source>
        <translation>default</translation>
    </message>
    <message>
        <location filename="../settingwindow.ui" line="355"/>
        <source>4:3</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settingwindow.ui" line="360"/>
        <source>16:9</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settingwindow.ui" line="365"/>
        <source>16:10</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settingwindow.ui" line="370"/>
        <source>1.85:1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settingwindow.ui" line="375"/>
        <source>2.35:1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settingwindow.ui" line="380"/>
        <location filename="../settingwindow.cpp" line="873"/>
        <source>custom</source>
        <translation>personalizzato</translation>
    </message>
    <message>
        <location filename="../settingwindow.ui" line="390"/>
        <source>1.33</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settingwindow.ui" line="397"/>
        <source>Apply scale</source>
        <translation>Rapporto di esecuzione</translation>
    </message>
    <message>
        <location filename="../settingwindow.ui" line="416"/>
        <source>X</source>
        <translation>Spostamento laterale</translation>
    </message>
    <message>
        <location filename="../settingwindow.ui" line="430"/>
        <source>Y</source>
        <translation>Spostamento verticale</translation>
    </message>
    <message>
        <location filename="../settingwindow.ui" line="444"/>
        <source>Width</source>
        <translation>larghezza</translation>
    </message>
    <message>
        <location filename="../settingwindow.ui" line="458"/>
        <source>Height</source>
        <translation>altezza</translation>
    </message>
    <message>
        <location filename="../settingwindow.ui" line="472"/>
        <source>Apply Custom Dimensions</source>
        <translation>Esegui dimensioni personalizzate</translation>
    </message>
    <message>
        <location filename="../settingwindow.ui" line="491"/>
        <source>githubWeb</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settingwindow.ui" line="498"/>
        <source>new(gitee)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settingwindow.ui" line="505"/>
        <source>new(github)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settingwindow.ui" line="512"/>
        <source>reprotBug</source>
        <translation>Segnala bug</translation>
    </message>
    <message>
        <location filename="../settingwindow.ui" line="535"/>
        <source>Spark-Hub(Test)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settingwindow.ui" line="542"/>
        <source>Dowload Wallpaper</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settingwindow.ui" line="553"/>
        <source>Advanced Settings</source>
        <translation>Interfaccia più impostazioni</translation>
    </message>
    <message>
        <location filename="../settingwindow.ui" line="560"/>
        <source>Wallpaper Local</source>
        <translation>Sfondo locale</translation>
    </message>
    <message>
        <location filename="../settingwindow.ui" line="567"/>
        <location filename="../settingwindow.cpp" line="123"/>
        <source>Wallpaper History</source>
        <translation>Sfondo storico</translation>
    </message>
    <message>
        <location filename="../settingwindow.ui" line="574"/>
        <source>Wallpaper Engine-plugin</source>
        <translation>Estensione motore di sfondo</translation>
    </message>
    <message>
        <location filename="../settingwindow.cpp" line="90"/>
        <source>Exit</source>
        <translation>quit</translation>
    </message>
    <message>
        <location filename="../settingwindow.cpp" line="111"/>
        <source>Screenshot</source>
        <translation>screenshot</translation>
    </message>
    <message>
        <location filename="../settingwindow.cpp" line="127"/>
        <source>Main View</source>
        <translation>Modulo principale</translation>
    </message>
    <message>
        <location filename="../settingwindow.cpp" line="144"/>
        <source>Help</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settingwindow.cpp" line="152"/>
        <source>github</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settingwindow.cpp" line="169"/>
        <source>Double click to open the configuration interface</source>
        <translation>Fare doppio clic per aprire l&apos;interfaccia</translation>
    </message>
    <message>
        <location filename="../settingwindow.cpp" line="222"/>
        <source>Latest version</source>
        <translation>Ultima versione</translation>
    </message>
</context>
<context>
    <name>view</name>
    <message>
        <location filename="../listview/view.cpp" line="244"/>
        <location filename="../listview/view.cpp" line="251"/>
        <source>Delete!!</source>
        <translation>Elimina!!</translation>
    </message>
    <message>
        <location filename="../listview/view.cpp" line="244"/>
        <source>The file does not exist. Do you want to delete it</source>
        <translation>Il file non esiste. Vuoi eliminarlo</translation>
    </message>
    <message>
        <location filename="../listview/view.cpp" line="251"/>
        <source>Delete all history imports ?</source>
        <translation>Vuoi eliminare tutto?</translation>
    </message>
</context>
<context>
    <name>wallpaperEnginePlugin</name>
    <message>
        <location filename="../listview/wallpaperengineplugin.ui" line="14"/>
        <source>Form</source>
        <translation>finestra</translation>
    </message>
    <message>
        <location filename="../listview/wallpaperengineplugin.ui" line="25"/>
        <source>Select Path</source>
        <translation>Seleziona tracciato</translation>
    </message>
    <message>
        <location filename="../listview/wallpaperengineplugin.ui" line="35"/>
        <source>Apply</source>
        <translation>implementare</translation>
    </message>
    <message>
        <location filename="../listview/wallpaperengineplugin.ui" line="56"/>
        <source>You have to provide the path of Wallpaper Engine from Steam. It doesn&apos;t work on Linux, but required functionality is present</source>
        <translation>È necessario installare il motore di carta da parati in Steam, altrimenti sarà inutile. Se lo installi sotto Linux, non puoi aprirlo, ma puoi attraversare il video</translation>
    </message>
    <message>
        <location filename="../listview/wallpaperengineplugin.cpp" line="157"/>
        <location filename="../listview/wallpaperengineplugin.cpp" line="171"/>
        <source>Error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../listview/wallpaperengineplugin.cpp" line="157"/>
        <location filename="../listview/wallpaperengineplugin.cpp" line="171"/>
        <source>Not containing the word steam!</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
